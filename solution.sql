CREATE DATABASE blog_db;

USE blog_db;

-- tables
CREATE TABLE users(
	id INT NOT NULL AUTO_INCREMENT,
    email VARCHAR(100) NOT NULL,
    pssword VARCHAR(300) NOT NULL,
    datetime_created DATETIME NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE posts(
	id INT NOT NULL AUTO_INCREMENT,
    user_id INT NOT NULL,
    title VARCHAR(500) NOT NULL,
    content VARCHAR(5000) NOT NULL,
    datetime_posted DATETIME NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_posts_user_id
    	FOREIGN KEY (user_id) REFERENCES users(id)
    	ON UPDATE CASCADE
    	ON DELETE RESTRICT
);

CREATE TABLE posts_comments(
	id INT NOT NULL AUTO_INCREMENT,
    user_id INT NOT NULL,
    post_id INT NOT NULL,
    content VARCHAR(5000) NOT NULL,
    datetime_commented DATETIME NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_posts_comments_user_id
    	FOREIGN KEY (user_id) REFERENCES users(id)
    	ON UPDATE CASCADE
    	ON DELETE RESTRICT,
    CONSTRAINT fk_posts_comments_post_id
    	FOREIGN KEY (post_id) REFERENCES posts(id)
    	ON UPDATE CASCADE
    	ON DELETE RESTRICT
);

CREATE TABLE posts_likes(
	id INT NOT NULL AUTO_INCREMENT,
    user_id INT NOT NULL,
    post_id INT NOT NULL,
    datetime_liked DATETIME NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_posts_likes_user_id
    	FOREIGN KEY (user_id) REFERENCES users(id)
    	ON UPDATE CASCADE
    	ON DELETE RESTRICT,
    CONSTRAINT fk_posts_likes_post_id
    	FOREIGN KEY (post_id) REFERENCES posts(id)
    	ON UPDATE CASCADE
    	ON DELETE RESTRICT
);

-- users
INSERT INTO users(email, pssword, datetime_created)
VALUES ("johnsmith@gmail.com", "passwordA", "2021-01-01T01:00:00");

INSERT INTO users(email, pssword, datetime_created)
VALUES ("juandelacruz@gmail.com", "passwordB", "2021-01-01T02:00:00");

INSERT INTO users(email, pssword, datetime_created)
VALUES ("janesmith@gmail.com", "passwordC", "2021-01-01T03:00:00");

INSERT INTO users(email, pssword, datetime_created)
VALUES ("mariadelacruz@gmail.com", "passwordD", "2021-01-01T04:00:00");

INSERT INTO users(email, pssword, datetime_created)
VALUES ("johndoe@gmail.com", "passwordE", "2021-01-01T05:00:00");

-- posts
INSERT INTO posts(title, content, datetime_posted, user_id)
VALUES ("First Code", "Hello World!", "2021-01-02T01:00:00", 1);

INSERT INTO posts(title, content, datetime_posted, user_id)
VALUES ("Second Code", "Hello Earth!", "2021-01-02T02:00:00", 1);

INSERT INTO posts(title, content, datetime_posted, user_id)
VALUES ("Third Code", "Welcome to Mars!", "2021-01-02T03:00:00", 2);

INSERT INTO posts(title, content, datetime_posted, user_id)
VALUES ("Fourth Code", "Bye bye solar system!", "2021-01-02T02:00:00", 4);

-- get all the title with a user ID of 1
SELECT title
FROM posts
WHERE user_id = 1;


-- get all the user's email and datetime of creation
SELECT email, datetime_created
FROM users;

-- Update a posts content by using record's ID
UPDATE posts
SET content = "Hello to the people of the Earth!"
WHERE content = "Hello Earth!";

-- delete a user "johndoe@gmail.com"
DELETE from users
WHERE email = "johndoe@gmail.com";